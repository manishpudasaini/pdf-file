package PdfReadWrite.PdfTask;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class PdfReadWrite {
    public static void main(String[] args) {
        try(PDDocument pdDocument = new PDDocument()){
            PDPage page = new PDPage();
            pdDocument.addPage(page);
            PDPageContentStream pcs = new PDPageContentStream(pdDocument,page);
            int pageHeight = (int) page.getTrimBox().getHeight();

            int cellHeight = 50;
            int cellWidth = 200;

            int xcord = 10;
            int ycord = pageHeight-(xcord+cellHeight);

            FileInputStream fileInputStream = new FileInputStream("Excel/student.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator rowIterator = sheet.rowIterator();
            int flag = 0;
            while (rowIterator.hasNext()){
                XSSFRow row = (XSSFRow) rowIterator.next();

                Iterator cellIterator = row.cellIterator();

                while (cellIterator.hasNext()){
                    XSSFCell cell = (XSSFCell) cellIterator.next();

                    pcs.addRect(xcord,ycord,cellWidth,cellHeight);  //table

                    pcs.beginText();  //for text

                    if(flag==0){
                        File file = new File("Font/java.ttf");  //for font style
                        PDFont font = PDType0Font.load(pdDocument,file);

                        pcs.setCharacterSpacing(2);
                        pcs.setFont(font,22);
                    }else {
                        pcs.setFont(PDType1Font.TIMES_ITALIC,18);
                    }

                    pcs.newLineAtOffset(xcord+10,ycord+10);  //text x y coordinate
                    String text= null;

                    switch(cell.getCellType()){
                        case STRING:
                            text = cell.getStringCellValue();
                            break;
                        case NUMERIC:
                             text = Double.toString(cell.getNumericCellValue()) ;
                             break;

                        default:
                            System.out.print("Default");
                            break;
                    }

                    pcs.showText(text);
                    pcs.endText();

                    xcord += cellWidth;

                }
                xcord = 10;
                ycord -= cellHeight;
                flag++;
            }

            pcs.stroke();
            pcs.close();
            
            pdDocument.save("Pdf/task_pdf.pdf");
            System.out.println("Operation completed");

        }
        catch (IOException exception){
            exception.printStackTrace();
        }

    }
}
